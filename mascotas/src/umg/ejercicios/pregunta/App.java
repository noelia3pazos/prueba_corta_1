package umg.ejercicios.pregunta;

/**
 * Created by alumno on 5/07/2017.
 */
public class App {

    public boolean salir;

    public static void main(String[] args) {

        System.out.println("Tienda de Mascotas");

        Tienda t = new Tienda();
        t.setGerente("Noelia Pazos");
        t.setDireccion("Guatemala");
        t.setAnno(2017);


        Mascota m1=new Mascota();
        m1.setTipo("Perro");
        m1.setRaza("Pitbull");
        m1.setEdad(2);
        t.addMascosta(m1);

        Mascota m2=new Mascota();
        m2.setTipo("Perro");
        m2.setRaza("Dalmata");
        m2.setEdad(8);
        t.addMascosta(m2);


        System.out.println("La suma de edades es " + t.sumaAnnosMascotas());









    }
}
