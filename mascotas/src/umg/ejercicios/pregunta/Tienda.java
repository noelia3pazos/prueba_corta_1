package umg.ejercicios.pregunta;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by alumno on 5/07/2017.
 */
public class Tienda {

    public String gerente;
    public String direccion;
    public int anno;
    private List <Mascota> mascotas;

    public Tienda() {
        mascotas= new ArrayList<>();
    }

    public Tienda(String gerente, String direccion, int anno) {
        this.gerente = gerente;
        this.direccion = direccion;
        this.anno = anno;
    }


    public String getGerente() {
        return gerente;
    }

    public void setGerente(String gerente) {
        this.gerente = gerente;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public void addMascosta(Mascota m) {
        mascotas.add(m);
    }

    public float sumaAnnosMascotas() {
        float suma = 0;

        for (Mascota m:mascotas) {
            float edad = m.getEdad();
            suma = suma + edad;
        }
        return suma;

    }
}
