package umg.ejercicios.pregunta;

/**
 * Created by alumno on 5/07/2017.
 */
public class Mascota {

    public String tipo;
    public String raza;
    public int edad;

    public Mascota(String tipo, String raza, int edad) {
        this.tipo = tipo;
        this.raza = raza;
        this.edad = edad;
    }

    public Mascota() {
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
}
